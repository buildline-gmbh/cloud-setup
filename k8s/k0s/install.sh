#!/bin/bash

curl -sSLf https://get.k0s.sh | sudo sh

k0s install controller --single --disable-components metrics-server
#k0s install controller --single --disable-components metrics-server -c config.yaml

k0s start
sleep 5

K0S_PID=$(systemctl show --property MainPID --value k0scontroller.service)

while [ "$(k0s status -o json | jq '.Pid')" != "$K0S_PID" ]; do
   sleep 5
   echo "Waiting for k0s to be ready."
done


while [ "$(k0s kubectl get pods -n kube-system -l=k8s-app='kube-proxy' -o jsonpath='{.items[*].status.containerStatuses[0].ready}')" != "true" ]; do
   sleep 5
   echo "Waiting for kube-proxy to be ready."
done

export KUBECONFIG=/var/lib/k0s/pki/admin.conf

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx && helm repo update
helm upgrade \
   -f ../ingress/values.yaml \
   --install ingress-nginx ingress-nginx \
   --repo https://kubernetes.github.io/ingress-nginx \
   --namespace kube-system