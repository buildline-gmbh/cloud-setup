#!/bin/bash

K3S_MANIFEST_DIR=/var/lib/rancher/k3s/server/manifests
K3S_CONFIG_DIR=/etc/rancher/k3s

mkdir -p $K3S_MANIFEST_DIR
mkdir -p $K3S_CONFIG_DIR

# skip traefik installation
touch $K3S_MANIFEST_DIR/traefik.yaml.skip

# create k3s config file
cat >$K3S_CONFIG_DIR/config.yaml <<EOL
datastore-endpoint: "postgres://tenant1@psql.buildline-cloud.de/tenant1"
datastore-certfile: "/etc/ssl/certs/tenant1.pem"
datastore-keyfile: "/etc/ssl/private/tenant1.pem"
EOL

curl -sfL https://get.k3s.io | sh -


while [ "$(k3s kubectl get pods -n kube-system -l=k8s-app='kube-dns' -o jsonpath='{.items[*].status.containerStatuses[0].ready}')" != "true" ]; do
   sleep 5
   echo "Waiting for kube-proxy to be ready."
done

# create lets encrypt tls secret
k3s kubectl create secret tls tls-cert --key /etc/ssl/private/tenant1.eu1.buildline-cloud.de.pem  --cert /etc/ssl/certs/tenant1.eu1.buildline-cloud.de.pem
# create mtls secret for api access
k3s kubectl create secret generic internal-ca-cert --from-file=ca.crt=/etc/ssl/certs/internal-ca.pem

export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx && helm repo update
helm upgrade \
   -f ../ingress/values.yaml \
   --install ingress-nginx ingress-nginx \
   --repo https://kubernetes.github.io/ingress-nginx \
   --namespace kube-system