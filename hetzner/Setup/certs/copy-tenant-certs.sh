#!/bin/bash
 # cert files
 scp -i ../tenant1_ssh.key -o StrictHostKeyChecking=no  client1.pem root@tenant1.eu1.buildline-cloud.de:/etc/ssl/certs/tenant1.pem
 scp -i ../tenant1_ssh.key -o StrictHostKeyChecking=no  fullchain.cer root@tenant1.eu1.buildline-cloud.de:/etc/ssl/certs/tenant1.eu1.buildline-cloud.de.pem

# key files
 scp -i ../tenant1_ssh.key -o StrictHostKeyChecking=no  client1-key.pem root@tenant1.eu1.buildline-cloud.de:/etc/ssl/private/tenant1.pem
 scp -i ../tenant1_ssh.key -o StrictHostKeyChecking=no  tenant1.eu1.buildline-cloud.de.key root@tenant1.eu1.buildline-cloud.de:/etc/ssl/private/tenant1.eu1.buildline-cloud.de.pem
