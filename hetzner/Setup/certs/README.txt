Needed Certs:

1. self signed client cert incl. key from BuildLine CA (used for authentication against psql DB)
  - client1.crt
  - client1-key.key

2. cert chain from BuildLine CA (handels access to ingress)
  - fullchain.crt

3. let's encrypt cert for domain {tenantName}.{region}.buildline-cloud.de (handels secure connection)
  - lets-encrypt.fullchain.crt
  - tenant1.eu1.buildline-cloud.de.key