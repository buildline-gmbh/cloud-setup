#!/bin/bash

clear

###############################################################################
##
## functions
##
###############################################################################

getValueFromJson(){
  local json=$1
  local path=$2
  echo $(echo $json | jq -r $path)
}

###############################################################################
##
## Variables
##
###############################################################################

source <(grep = script_config.ini)
export HCLOUD_TOKEN=$REGION_API_KEY
TENANT_SUBDOMAIN=$(echo "$TENANT_NAME.$REGION" | tr '[:upper:]' '[:lower:]')
CLOUD_CONFIG_FILE="./${K8S_KIND}_cloud_config.yaml"


echo "
###################################################################################
##
## Generate ssh key
##
###################################################################################
"

SSH_KEY_FILE="${TENANT_NAME}_ssh.key"
SSH_PUB_FILE="${TENANT_NAME}_ssh.key.pub"

rm -f "$SSH_KEY_FILE"
rm -f "$SSH_PUB_FILE"

ssh-keygen \
  -q \
  -t rsa \
  -b 2048 \
  -C "$TENANT_NAME" \
  -N "$TENANT_SECRET" \
  -f "$SSH_KEY_FILE"

echo -e "$SSH_KEY_FILE created"
echo -e "$SSH_PUB_FILE created"

echo "
###################################################################################
##
## Add ssh key to project / region
##
###################################################################################
"

hcloud ssh-key create \
  --name "$TENANT_NAME" \
  --public-key-from-file "$SSH_PUB_FILE" \
  --label "tenant=${TENANT_NAME}"

SSH_JSON=$(hcloud ssh-key describe "$TENANT_NAME" -o "json") 
SSH_ID=$(getValueFromJson "$SSH_JSON" ".id")

echo ""
echo -e "SSH ID:\t $SSH_ID"