#!/bin/bash

source ./base.sh

echo "
###################################################################################
##
## Add Server
##
###################################################################################
"

hcloud server create \
  --name "$TENANT_NAME" \
  --location "$LOCATION" \
  --type "cpx11" \
  --image "ubuntu-22.04" \
  --ssh-key "$SSH_ID" \
  --label "tenant=${TENANT_NAME}" \
  --label "role=worker" \
  --user-data-from-file "$CLOUD_CONFIG_FILE"

SERVER_JSON=$(hcloud server describe "$TENANT_NAME" -o "json") 
SERVER_IP_V4=$(getValueFromJson "$SERVER_JSON"  ".public_net.ipv4.ip")
SERVER_IP_V6=$(getValueFromJson "$SERVER_JSON" ".public_net.ipv6.ip")
SERVER_ID=$(getValueFromJson "$SERVER_JSON" ".id")

echo ""
echo -e "SERVER ID:\t $SERVER_ID"
echo -e "SERVER IPV4:\t $SERVER_IP_V4"
echo -e "SERVER IPV6:\t $SERVER_IP_V6"


echo "
###################################################################################
"

echo "
###################################################################################
##
## SET DNS Records
##
###################################################################################
"

# TODO set IPv6 DNS Entry 
# Use Bulk insert
# POST https://dns.hetzner.com/api/v1/records/bulk
# {
#   "records": [
#     {
#         "zone_id": "$DNS_ZONE_ID",
#         "type": "A",
#         "name": "$TENANT_SUBDOMAIN",
#         "value": "$LB_IP_V4",
#         "ttl": 60
#     },
#     {
#         "zone_id": "$DNS_ZONE_ID",
#         "type": "AAAA",
#         "name": "$TENANT_SUBDOMAIN",
#         "value": "$LB_IP_V6",
#         "ttl": 60
#     }
#   ]
# }


curl -X "POST" "https://dns.hetzner.com/api/v1/records" \
     -H "Content-Type: application/json" \
     -H "Auth-API-Token: $DNS_API_KEY" \
     -d "{ \"value\": \"$SERVER_IP_V4\", \"ttl\": 60, \"type\": \"A\", \"name\": \"$TENANT_SUBDOMAIN\", \"zone_id\": \"$DNS_ZONE_ID\" }"




echo "Finito"