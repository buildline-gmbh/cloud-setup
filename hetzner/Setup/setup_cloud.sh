#!/bin/bash

source ./base.sh

echo "
###################################################################################
##
## get tls cert id
##
###################################################################################
"

TLS_JSON=$(hcloud certificate describe "default" -o json)
TLS_ID=$(getValueFromJson "$TLS_JSON" ".id")

echo -e "TLS ID:\t $TLS_ID"

echo "
###################################################################################
##
## Add Network
##
###################################################################################
"

hcloud network create \
  --name "$TENANT_NAME" \
  --ip-range "10.0.0.0/24" \
  --label "tenant=${TENANT_NAME}" 

hcloud network add-subnet "$TENANT_NAME" \
  --network-zone "$ZONE" \
  --type "server" \
  --ip-range "10.0.0.0/24"

echo "
###################################################################################
##
## Add Loadbalancer
##
###################################################################################
"

hcloud load-balancer create \
  --name "$TENANT_NAME" \
  --location "$LOCATION" \
  --type "lb11"

hcloud load-balancer attach-to-network "$TENANT_NAME" \
  --network "$TENANT_NAME" \
  --ip "10.0.0.254"  

hcloud load-balancer add-target "$TENANT_NAME" \
  --label-selector "role=worker" \
  --use-private-ip

hcloud load-balancer add-service "$TENANT_NAME" \
  --http-certificates "$TLS_ID" \
  --http-redirect-http \
  --listen-port 443 \
  --protocol "https" \
  --destination-port 32080

hcloud load-balancer update-service "$TENANT_NAME" \
  --destination-port 32080 \
  --listen-port 443 \
  --health-check-http-path "/healthz" \
  --health-check-protocol "http" \
  --health-check-port 32080

LB_JSON=$(hcloud load-balancer describe "$TENANT_NAME" -o "json") 
LB_IP_V4=$(getValueFromJson "$LB_JSON"  ".public_net.ipv4.ip")
LB_IP_V6=$(getValueFromJson "$LB_JSON" ".public_net.ipv6.ip")
LB_ID=$(getValueFromJson "$LB_JSON" ".id")

echo ""
echo -e "LB ID:\t $LB_ID"
echo -e "LB IPV4:\t $LB_IP_V4"
echo -e "LB IPV6:\t $LB_IP_V6"

echo "
###################################################################################
##
## SET DNS Records
##
###################################################################################
"

# TODO set IPv6 DNS Entry 
# Use Bulk insert
# POST https://dns.hetzner.com/api/v1/records/bulk
# {
#   "records": [
#     {
#         "zone_id": "$DNS_ZONE_ID",
#         "type": "A",
#         "name": "$TENANT_SUBDOMAIN",
#         "value": "$LB_IP_V4",
#         "ttl": 60
#     },
#     {
#         "zone_id": "$DNS_ZONE_ID",
#         "type": "AAAA",
#         "name": "$TENANT_SUBDOMAIN",
#         "value": "$LB_IP_V6",
#         "ttl": 60
#     }
#   ]
# }


curl -X "POST" "https://dns.hetzner.com/api/v1/records" \
     -H "Content-Type: application/json" \
     -H "Auth-API-Token: $DNS_API_KEY" \
     -d "{ \"value\": \"$LB_IP_V4\", \"ttl\": 60, \"type\": \"A\", \"name\": \"$TENANT_SUBDOMAIN\", \"zone_id\": \"$DNS_ZONE_ID\" }"


echo "
###################################################################################
##
## Add Server
##
###################################################################################
"

hcloud server create \
  --location "$LOCATION" \
  --type "cpx11" \
  --name "$TENANT_NAME" \
  --image "ubuntu-22.04" \
  --ssh-key "$SSH_ID" \
  --network "$TENANT_NAME" \
  --label "tenant=${TENANT_NAME}" \
  --label "role=worker" \
  --user-data-from-file "$CLOUD_CONFIG_FILE"

SERVER_JSON=$(hcloud server describe "$TENANT_NAME" -o "json") 
SERVER_IP_V4=$(getValueFromJson "$SERVER_JSON"  ".public_net.ipv4.ip")
SERVER_IP_V6=$(getValueFromJson "$SERVER_JSON" ".public_net.ipv6.ip")
SERVER_ID=$(getValueFromJson "$SERVER_JSON" ".id")

echo ""
echo -e "SERVER ID:\t $SERVER_ID"
echo -e "SERVER IPV4:\t $SERVER_IP_V4"
echo -e "SERVER IPV6:\t $SERVER_IP_V6"


echo "
###################################################################################
"

echo "Finito"