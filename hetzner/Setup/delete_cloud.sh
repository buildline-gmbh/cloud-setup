#!/bin/bash

source ./base.sh

hcloud load-balancer delete "$TENANT_NAME"
hcloud server delete "$TENANT_NAME"
hcloud network delete "$TENANT_NAME"
hcloud ssh-key delete "$TENANT_NAME"

DNS_JSON=$(
      curl "https://dns.hetzner.com/api/v1/records?zone_id=$DNS_ZONE_ID" \
        -H "Content-Type: application/json" \
        -H "Auth-API-Token: $DNS_API_KEY" \
    )

echo $DNS_JSON

DNS_RECORD_ID=$(getValueFromJson "$DNS_JSON" ".records[]|select(.name==\"$TENANT_SUBDOMAIN\").id")

# TODO remove both IPv4 & IPv6
echo $DNS_RECORD_ID

curl -X "DELETE" "https://dns.hetzner.com/api/v1/records/$DNS_RECORD_ID" \
     -H "Content-Type: application/json" \
     -H "Auth-API-Token: $DNS_API_KEY" \
     -w "%{http_code}"