#!/bin/bash

cfssl gencert -initca configs/ca.json | cfssljson -bare ca
cfssl gencert -initca configs/intermediate-ca.json | cfssljson -bare intermediate_ca
cfssl sign -ca ca.pem -ca-key ca-key.pem -config configs/cfssl.json -profile intermediate_ca intermediate_ca.csr | cfssljson -bare intermediate_ca

######
#cfssl gencert -ca intermediate_ca.pem -ca-key intermediate_ca-key.pem -config configs/cfssl.json -profile=peer configs/host.json | cfssljson -bare peer1
#cfssl gencert -ca intermediate_ca.pem -ca-key intermediate_ca-key.pem -config configs/cfssl.json -profile=server configs/host.json | cfssljson -bare server1
cfssl gencert -ca intermediate_ca.pem -ca-key intermediate_ca-key.pem -config configs/cfssl.json -profile=client configs/host.json | cfssljson -bare client1

cat ca.pem intermediate_ca.pem > fullchain.pem